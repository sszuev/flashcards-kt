### flashcard-kt ::: stubs

Collection of stub-requests to be used while testing (both manual and unit).
See:

- [Stubs.kt](src/main/kotlin/Stubs.kt) for unit testing
- [card-requests.rest](card-requests.rest) for manual testing cards api
- [dictionary-requests.rest](dictionary-requests.rest) for manual testing dictionaries api