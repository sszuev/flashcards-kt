## OpenTutor - a flashcard system to learn words

A simple opensource application that provides flashcards to learn foreign words.    
It is supposed to be similar to Lingvo Tutor.

#### It is a training project, which is used to demonstrate different technologies and approaches.

## OpenTutor - MVP

MVP exists and is runnable, the project home is https://gitlab.com/sszuev/flashcards
