package com.gitlab.sszuev.flashcards.dbmem.documents.impl

/**
 * Created by @ssz on 02.05.2021.
 *
 * @see [wiki: Part of speech](https://en.wikipedia.org/wiki/Part_of_speech)
 */
@Suppress("unused")
enum class StandardPartOfSpeech {
    NOUN, ADJECTIVE, VERB, ADVERB, PRONOUN, PREPOSITION, CONJUNCTION, INTERJECTION, ARTICLE
}

