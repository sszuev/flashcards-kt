package com.gitlab.sszuev.flashcards.dbmem.documents.impl

enum class StandardLanguage {
    EN, RU
}