package com.gitlab.sszuev.flashcards.dbmem.dao

data class Example(
    val id: Long,
    val cardId: Long,
    val text: String,
)