package com.gitlab.sszuev.flashcards.dbmem.dao

data class Language(
    val id: String,
    val partsOfSpeech: String,
)